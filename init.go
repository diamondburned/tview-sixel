package tviewsixel

import "github.com/mattn/go-tty"

var (
	// Cell size in pixels
	CharW, CharH int
)

// function runs when the app starts, calculating cell size
func init() {
	t, err := tty.Open()
	if err != nil {
		panic(err)
	}

	defer t.Close()

	c, l, w, h, err := t.SizePixel()
	if err != nil {
		panic(err)
	}

	if w == 0 || h == 0 {
		panic("Error getting screen size!")
	}

	CharW, CharH = w/(c-1), h/(l-1)
}
