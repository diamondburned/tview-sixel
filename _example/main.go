package main

import (
	"image"
	_ "image/jpeg"
	"net/http"

	"github.com/Bios-Marcel/tview"
	tviewsixel "gitlab.com/diamondburned/tview-sixel"
)

func main() {
	r, err := http.Get("https://golang.org/doc/gopher/pencil/gophermega.jpg")
	if err != nil {
		panic(err)
	}

	defer r.Body.Close()

	img, _, err := image.Decode(r.Body)
	if err != nil {
		panic(err)
	}

	flex := tview.NewFlex()

	tv := tview.NewTextView()
	tv.SetText("Hello, world!")
	tv.SetBackgroundColor(-1)

	flex.AddItem(tv, 0, 1, false)

	p, err := tviewsixel.NewPicture(img)
	if err != nil {
		panic(err)
	}

	p.ChangeAlignment(tviewsixel.AlignMiddle)

	flex.AddItem(p, 0, 2, true)

	if err := tview.NewApplication().SetRoot(flex, true).Run(); err != nil {
		panic(err)
	}
}
