package tviewsixel

import (
	"bytes"
	"fmt"
	"image"

	"github.com/Bios-Marcel/tview"
	"github.com/gdamore/tcell"
	"github.com/mattn/go-sixel"
	"github.com/nfnt/resize"
)

// Picture is the primitive that draws w3m
type Picture struct {
	x, y, width, height int

	align                 Align
	alignBottomCompensate int

	visible bool
	focus   tview.Focusable

	state       []byte
	originalImg image.Image
}

// NewPicture makes a new picture
func NewPicture(img image.Image) (*Picture, error) {
	p := &Picture{}
	p.focus = p
	p.originalImg = img
	p.alignBottomCompensate = 3
	p.visible = true

	return p, nil
}

// ChangeAlignment changes how the picture is aligned
func (p *Picture) ChangeAlignment(a Align) {
	p.align = a
}

// AlignBotomCompensate changes the lines AlignButtom
// should move the image up to prevent new lines from
// being printed. I have no idea how this works, and
// 3 (default) usually works for me.
func (p *Picture) AlignBotomCompensate(i int) {
	p.alignBottomCompensate = i
}

// Draw draws the w3m image constantly
func (p *Picture) Draw(screen tcell.Screen) (draw bool) {
	if p.width <= 0 || p.height <= 0 {
		return
	}

	draw = true

	imgW, imgH := p.CalculateSize()

	i := resize.Resize(
		uint(imgW), uint(imgH),
		p.originalImg,
		resize.Bilinear,
	)

	var b bytes.Buffer

	enc := sixel.NewEncoder(&b)
	enc.Dither = false

	if err := enc.Encode(i); err != nil {
		panic(err)
	}

	p.state = b.Bytes()

	switch p.align {
	case AlignTop:
		screen.ShowCursor(p.x, p.y)
	case AlignMiddle:
		realH := i.Bounds().Dy()
		screen.ShowCursor(p.x, p.y+p.height/2-realH/CharH/2-1)
	case AlignBottom:
		realH := i.Bounds().Dy()
		// 3 is the lowest I was able to go
		screen.ShowCursor(p.x, p.y+p.height-realH/CharH-p.alignBottomCompensate)
	}

	fmt.Print(string(p.state))

	return
}

// CalculateSize returns a proper resolution scaled to
// fit the container, while maintaining aspect ratio.
func (p *Picture) CalculateSize() (int, int) {
	maxW, maxH := p.width*CharW, p.height*CharH

	bounds := p.originalImg.Bounds()
	origW, origH := bounds.Dx(), bounds.Dy()
	imgW, imgH := origW, origH

	if maxW >= origW && maxH >= origH {
		return imgW, imgH
	}

	if origW > maxW {
		imgH = origH * maxW / origW
		if imgH < 1 {
			imgH = 1
		}

		imgW = maxW
	}

	if imgH > maxH {
		imgW = imgW * maxH / imgH
		if imgW < 1 {
			imgW = 1
		}

		imgH = maxH
	}

	return imgW, imgH
}

// SetVisible sets visibility like CSS.
func (p *Picture) SetVisible(v bool) {
	p.visible = v
}

// IsVisible returns visible
func (p *Picture) IsVisible() bool {
	return p.visible
}

// GetRect returns the rectangle dimensions
func (p *Picture) GetRect() (int, int, int, int) {
	return p.x, p.y, p.width, p.height
}

// SetRect sets the rectangle dimensions
func (p *Picture) SetRect(x, y, width, height int) {
	p.x = x
	p.y = y
	p.width = width
	p.height = height
}

// InputHandler sets no input handler, satisfying Primitive
func (p *Picture) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return nil
}

// Focus does nothing, really.
func (*Picture) Focus(delegate func(tview.Primitive)) {}

// Blur also does nothing.
func (*Picture) Blur() {}

// HasFocus always returns false, as you can't focus on this.
func (*Picture) HasFocus() bool {
	return false
}

// GetFocusable does whatever the fuck I have no idea
func (p *Picture) GetFocusable() tview.Focusable {
	return p.focus
}

func (p *Picture) SetOnFocus(func()) {}
func (p *Picture) SetOnBlur(func())  {}
