module gitlab.com/diamondburned/tview-sixel

require (
	github.com/Bios-Marcel/tview v0.0.0-20190405085044-a91fccfc5cec
	github.com/gdamore/tcell v1.1.1
	github.com/lucasb-eyer/go-colorful v1.0.2 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mattn/go-sixel v0.0.0-20190320171103-a8fac8fa7d81
	github.com/mattn/go-tty v0.0.0-20190418143243-a87bf4b22d6e
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/rivo/tview v0.0.0-20190406182340-90b4da1bd64c
	github.com/rivo/uniseg v0.0.0-20190313204849-f699dde9c340 // indirect
	github.com/soniakeys/quant v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20190322080309-f49334f85ddc // indirect
)
