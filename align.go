package tviewsixel

// Align is used for changing the picture's alignments
type Align int

const (
	// AlignTop aligns the image to the top
	AlignTop Align = iota
	// AlignMiddle aligns the image to the middle
	AlignMiddle
	// AlignBottom is self explanatory
	AlignBottom
)
